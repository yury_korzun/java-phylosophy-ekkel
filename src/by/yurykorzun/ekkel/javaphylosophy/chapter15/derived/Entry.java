package by.yurykorzun.ekkel.javaphylosophy.chapter15.derived;

import java.util.ArrayList;
import java.util.List;

public class Entry {

    public static void main(String[] args) {
        List<Number> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(0.4);
        numbers.add(0.34f);
        numbers.add(0x001);
        numbers.add(new Byte("3"));
        byte b = 4;
        numbers.add(b);
        System.out.println(numbers);
        for (Number i : numbers){
            System.out.println(i.getClass().getSimpleName());
        }
    }

}
