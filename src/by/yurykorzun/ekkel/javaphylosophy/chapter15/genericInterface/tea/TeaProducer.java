package by.yurykorzun.ekkel.javaphylosophy.chapter15.genericInterface.tea;

import by.yurykorzun.ekkel.javaphylosophy.chapter15.genericInterface.Producer;

import java.util.*;
import java.util.function.Consumer;

public class TeaProducer implements Producer<Tea>, Iterable<Tea> {

    private final Class[] types = {BlackTea.class, GreenTea.class, Puer.class,};
    private final Random random = new Random(new Random().nextInt());

    @Override
    public Tea next() {
        try {
            return (Tea) types[random.nextInt(types.length)].newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private class TeaIterator implements Iterator<Tea>{

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Tea next() {
            return null;
        }
    }

    @Override
    public Iterator<Tea> iterator() {
        return null;
    }

    @Override
    public void forEach(Consumer<? super Tea> action) {

    }

    @Override
    public Spliterator<Tea> spliterator() {
        return null;
    }
}
