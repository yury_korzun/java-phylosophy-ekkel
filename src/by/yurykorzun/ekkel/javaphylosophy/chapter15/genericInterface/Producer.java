package by.yurykorzun.ekkel.javaphylosophy.chapter15.genericInterface;

public interface Producer<T> {

    T next();

}
