package by.yurykorzun.ekkel.javaphylosophy.chapter15.selfBounded;

public class SelfBoundance {

    abstract static class SelfBounded<T extends SelfBounded<T>> {

        abstract T abstractMethod(T obj);

        T nonabstractMethod(T obj){
            System.out.println(obj.toString());
            return abstractMethod(obj);
        }

    }

    static class SelfBoundedDerived extends SelfBounded<SelfBoundedDerived> {

        @Override
        SelfBoundedDerived abstractMethod(SelfBoundedDerived obj) {
            System.out.println(obj.toString());
            return obj;
        }
    }

    public static void main(String[] args) {
        SelfBoundedDerived selfBoundedDerived = new SelfBoundedDerived();
        selfBoundedDerived.nonabstractMethod(selfBoundedDerived);
    }

}
