package by.yurykorzun.ekkel.javaphylosophy.chapter15.wildcards;

public class Wildcards {

    static void rawArgs(Holder holder, Object arg){
        holder.set(arg);
        holder.set(new Wildcards());
//        T t = holder.get();
    }

}
