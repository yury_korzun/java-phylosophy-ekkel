package by.yurykorzun.ekkel.javaphylosophy.chapter21.atomicity;

public class Atomicity {
    int i;
    void f1(){
        i++;
    }
    void f2(){
        i += 3;
    }
}
