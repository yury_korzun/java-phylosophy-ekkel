package by.yurykorzun.ekkel.javaphylosophy.chapter21.atomicity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AtomicityTest implements Runnable {

    protected int i = 0;
    public int get(){return i;}
    private synchronized void evenInc(){ i++; i++; }

    @Override
    public void run() {
        while(true){
            evenInc();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        AtomicityTest at = new AtomicityTest();
        executorService.execute(at);
//        TimeUnit.MILLISECONDS.sleep(1000);
        while(true){
            int val = at.get();
            if (val % 2 != 0){
                System.out.println(val);
                System.exit(0);
            }
            if (val % 1000000 == 0){
                System.out.println(val);
            }
        }
    }
}
