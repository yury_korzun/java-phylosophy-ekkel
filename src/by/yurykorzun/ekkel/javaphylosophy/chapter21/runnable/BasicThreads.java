package by.yurykorzun.ekkel.javaphylosophy.chapter21.runnable;

import by.yurykorzun.ekkel.javaphylosophy.chapter21.LiftOff;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

interface LiftExecutor {};

public class BasicThreads {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<LiftExecutor> executables[] = new Class[]{
                BasicThread.class,
                MainThread.class,
                MoreThreads.class,
                ThreadPoolFixed.class,
        };
        for (Class<LiftExecutor> cls : executables){
            Method main = cls.getMethod("main", String[].class);
            String[] params = null;
            main.invoke(null, (Object) params);
            System.out.println("=====================================================================================");
            System.out.println(cls.getSimpleName());
        }

    }

}

class BasicThread implements LiftExecutor {
    public static void main(String[] args) {
        Thread t = new Thread(new LiftOff());
        t.start();
        System.out.println("Waiting for lift off");
    }
}

class MainThread implements LiftExecutor {
    public static void main(String[] args) {
        LiftOff launch = new LiftOff();
        launch.run();
    }
}

class MoreThreads implements LiftExecutor {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++){
            new Thread(new LiftOff()).start();
        }
        System.out.println("Waiting for liftoff");
    }
}

class ThreadPoolFixed implements LiftExecutor {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++){
            executorService.execute(new LiftOff());
        }
        executorService.shutdown();
    }
}