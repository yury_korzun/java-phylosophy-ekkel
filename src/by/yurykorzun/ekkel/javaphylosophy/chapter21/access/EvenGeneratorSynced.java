package by.yurykorzun.ekkel.javaphylosophy.chapter21.access;

public class EvenGeneratorSynced extends EvenGenerator {

    @Override
    public synchronized int next() {
        ++currentValue;
        Thread.yield();
        ++currentValue;
        return currentValue;
    }

    public static void main(String[] args) {
        EvenChecker.test(new EvenGeneratorSynced());
    }
}
