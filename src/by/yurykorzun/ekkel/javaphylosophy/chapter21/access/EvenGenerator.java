package by.yurykorzun.ekkel.javaphylosophy.chapter21.access;

import by.yurykorzun.ekkel.javaphylosophy.chapter21.IntGenerator;

public class EvenGenerator extends IntGenerator {

    protected int currentValue = 0;

    @Override
    public int next() {
        ++currentValue;
        ++currentValue;
        return currentValue;
    }

    public static void main(String[] args) {
        EvenChecker.test(new EvenGenerator());
    }
}
