package by.yurykorzun.ekkel.javaphylosophy.chapter21.future;

import by.yurykorzun.ekkel.javaphylosophy.chapter21.TaskWithResult;

import java.util.ArrayList;
import java.util.concurrent.*;

public class FutureSample {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        ArrayList<Future<String>> results = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            results.add(executorService.submit(new TaskWithResult(i)));
        }
        for (Future<String> fs : results){
            try {
                System.out.println(fs.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            } catch (ExecutionException e) {
                e.printStackTrace();
            } finally {
                executorService.shutdown();
            }
        }
    }

}
