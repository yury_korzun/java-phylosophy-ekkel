package by.yurykorzun.ekkel.javaphylosophy.chapter7.noargs;

public class Root extends A {
    {
        new Block();
    }
    static {
        new StaticBlock();
    }
    B b = new B();

    public static void main(String[] args) {
        new Root();
    }
}
