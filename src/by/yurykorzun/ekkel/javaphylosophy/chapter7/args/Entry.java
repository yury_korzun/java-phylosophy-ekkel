package by.yurykorzun.ekkel.javaphylosophy.chapter7.args;

public class Entry {
    public static void main(String[] args) {
        new ChildObject();
//        new ChildRoot();
        new ChildRoot(1);
    }
}
