package by.yurykorzun.ekkel.javaphylosophy.chapter7.args;

public class ChildRoot extends Root {
    public ChildRoot(int i){
        super(i);
        System.out.println("this is " + this.getClass());
        System.out.println(i);
    }
}
