package by.yurykorzun.ekkel.javaphylosophy.chapter7.args;

class Root {
    protected Root(){
        System.out.println(Root.class);
    }
    protected Root(int i){
        this();
        System.out.println(i);
    }
}
