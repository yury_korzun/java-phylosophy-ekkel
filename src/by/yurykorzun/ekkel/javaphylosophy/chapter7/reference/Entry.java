package by.yurykorzun.ekkel.javaphylosophy.chapter7.reference;

public class Entry {
    private static void changeReference(Referenced obj){
        obj.inc();
        obj = new Referenced(42);
        obj.inc();
        obj.printSelf();
    }
    public static void main(String[] args) {
        Referenced obj = new Referenced(0);
        obj.inc();
        obj.printSelf();
        changeReference(obj);
        obj.printSelf();
    }
}
