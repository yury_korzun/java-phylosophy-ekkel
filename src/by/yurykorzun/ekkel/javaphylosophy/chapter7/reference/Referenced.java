package by.yurykorzun.ekkel.javaphylosophy.chapter7.reference;

public class Referenced {
    private int i;
    Referenced(int n){
        i = n;
    }
    public void inc(){
        i++;
    }
    public void printSelf(){
        System.out.println(this + " " + i);
    }
}
