package by.yurykorzun.ekkel.javaphylosophy.chapter7.overridingFinals;

public class Overriding extends WithFinals{

    private final void f(){
        System.out.println(Overriding.class + " I`m inaccesible!");
    }
}
