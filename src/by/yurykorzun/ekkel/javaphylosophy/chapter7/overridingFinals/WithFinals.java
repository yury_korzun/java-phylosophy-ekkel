package by.yurykorzun.ekkel.javaphylosophy.chapter7.overridingFinals;

public class WithFinals {
    private final void f(){
        System.out.println(WithFinals.class + " I`m inaccesible!");
    }
}
