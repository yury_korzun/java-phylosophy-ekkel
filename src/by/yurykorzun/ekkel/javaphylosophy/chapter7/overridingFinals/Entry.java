package by.yurykorzun.ekkel.javaphylosophy.chapter7.overridingFinals;

public class Entry {
    public static void main(String[] args) {
        OverridingPublic ovp = new OverridingPublic();
        ovp.f();
        Overriding ovf = ovp;
//        ovf.f();
        WithFinals wf = ovp;
//        wf.f();
    }
}
