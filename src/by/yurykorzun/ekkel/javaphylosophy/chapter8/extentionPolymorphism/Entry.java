package by.yurykorzun.ekkel.javaphylosophy.chapter8.extentionPolymorphism;

class Base {

    public static String staticGet(){
        return "Base static";
    }

    public String dynamicGet(){
        return "Base dynamic";
    }
}

class Derived extends Base {

    public static String staticGet(){
        return "Derived static";
    }

    public String dynamicGet(){
        return "Derived dynamic";
    }

}

public class Entry {

    public static void main(String[] args) {
        Base cls = new Derived();
        System.out.println(cls.staticGet());
        System.out.println(cls.dynamicGet());
        System.out.println(((Derived) cls).staticGet());
        System.out.println(((Derived) cls).dynamicGet());
    }

}
