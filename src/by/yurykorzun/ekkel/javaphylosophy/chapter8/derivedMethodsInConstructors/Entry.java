package by.yurykorzun.ekkel.javaphylosophy.chapter8.derivedMethodsInConstructors;

public class Entry {

    public static void main(String[] args) {
        new BadDerived(1);
        new GoodDerived(1);
    }

}

class BadBase {

    public void echo(){
        System.out.println("BadBase echo");
    }

    BadBase(){
        System.out.println("BadBase constructor");
        echo();
        System.out.println("BadBase constructor end");
    }

}

class BadDerived extends BadBase {

    private int i;

    BadDerived(int n){
        i = n;
    }

    public void echo(){
        System.out.println("BadDerived echo");
    }

}

class GoodBase {

    public void echo(){
        System.out.println("GoodBase echo");
    }

    GoodBase(){
        System.out.println("GoodBase constructor");
        ((GoodBase) this).echo();                           // doesn`t help
        System.out.println("GoodBase constructor end");
    }

}

class GoodDerived extends GoodBase {

    private int i;

    GoodDerived(int n){
        i = n;
    }

    public void echo(){
        System.out.println("GoodDerived echo");
    }

}