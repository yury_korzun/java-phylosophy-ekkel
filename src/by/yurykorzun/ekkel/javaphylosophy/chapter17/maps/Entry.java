package by.yurykorzun.ekkel.javaphylosophy.chapter17.maps;

import java.util.LinkedList;
import java.util.Map;

public class Entry {

    class  MyMap <K, V> {

        private final int SIZE = 997;

        @SuppressWarnings("unchecked")
        void method(K k, V v){

            LinkedList<Map.Entry<K, V>>[] castedList = new LinkedList[SIZE];
            LinkedList<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>();
            castedList[0] = list;

        }

    }

    public static void main(String[] args) {

        Object o = new Integer[]{ 1, 3};
        System.out.println(o);

        for (Object sub : (Object[])o){
            System.out.println(sub);
            System.out.println(sub.getClass());
        }

    }

}
