package by.yurykorzun.ekkel.javaphylosophy.chapter17.reflection;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Tricky {

    private static class GenericList extends ArrayList<Number> {}

    private static class GenericListExtended extends GenericList {}

    private static Optional<Class> optCls;

    private static void testReflectionUtilsPrototype(Object o){
        optCls = ReflectionUtilsPrototype.getGenericParameterClass(o.getClass());
        System.out.println(optCls.isPresent() ? optCls.get()
                                              : String.format("no class present for %s", o.getClass()));
    }

    public static void main(String[] args) {

        // adding
        List genList = new GenericList();
        List genListChild = new GenericListExtended();
        genList.add(1);
        genList.add(1.0);
        genList.add(1L);
        System.out.println(genList);
        // System.out.println(Arrays.toString(list));
        System.out.println(genList.get(2) instanceof Long);

        // reflection
        testReflectionUtilsPrototype(genList);
        testReflectionUtilsPrototype(genListChild);

        //
        System.out.println(ReflectionUtils.getGenericParameterClass(
                GenericIerarchy.E.class,
                GenericIerarchy.A.class,
                0));
    }

}
