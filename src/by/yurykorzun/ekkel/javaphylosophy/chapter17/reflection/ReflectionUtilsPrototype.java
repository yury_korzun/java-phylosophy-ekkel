package by.yurykorzun.ekkel.javaphylosophy.chapter17.reflection;

import com.sun.istack.internal.Nullable;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

public class ReflectionUtilsPrototype {

    @Nullable
    public static Optional<Class> getGenericParameterClass(Class actualClass, int paramIndex){

        ParameterizedType ptype;
        try {
            ptype = (ParameterizedType) actualClass.getGenericSuperclass();
        } catch (ClassCastException cce){
            // throw new RuntimeException("Class is not parametrized");
            return Optional.empty();
        }

        Type[] types = ptype.getActualTypeArguments();
        if (paramIndex > types.length - 1)
            throw new RuntimeException("Type index out of bounds");
        Class genClass = (Class) types[paramIndex];
        return Optional.of(genClass);
    }

    public static Optional<Class> getGenericParameterClass(Class actualClass){
        return getGenericParameterClass(actualClass, 0);
    }
}
