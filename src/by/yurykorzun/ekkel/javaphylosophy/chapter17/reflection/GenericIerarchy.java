package by.yurykorzun.ekkel.javaphylosophy.chapter17.reflection;

import java.util.Collection;
import java.util.Set;

public class GenericIerarchy {

    static class A< K,
                    L>
    {}

    static class B< P,
                    Q,
                    R extends Collection>

            extends A<  Q,
                        P>
    {}

    static class C< X extends Comparable<String>,
                    Y,
                    Z>

            extends B<  Z,
                        X,
                        Set<Long>>
    {}

    static class D< M,
                    N extends Comparable<Double>>

            extends C<  String,
                        N,
                        M>
    {}

    static class E
            extends D<  Integer,
                        Double>
    {}

}
